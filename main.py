import os

cur_dir = os.path.dirname(os.path.abspath(__file__))
dlls_dir = os.path.join(cur_dir, "dlls")
os.environ["PYSDL2_DLL_PATH"] = dlls_dir

import paper
import paper.app
import paper.widgets as wg

from paper.fonts import FontManager


def main():
    fn = FontManager("fonts/")
    roboto = fn.roboto()
    font = roboto.size(14).style("Light").get()

    app = paper.app.App()
    window = app.init_window("Привет", 800, 600)

    widget = wg.Label("Привет, Label!", font)
    widget = wg.Button("BUTTON", roboto)
    window.set_widget(widget)
    app.run_events()
    return 0


if __name__ == "__main__":
    main()
