#!/usr/bin/env python
# -*- coding: utf-8 -*-
import ctypes

import sdl2

from paper.window import Window

__author__ = 'cjaym'


class App:
    def __init__(self):
        self.window = None
        sdl2.SDL_Init(sdl2.SDL_INIT_VIDEO)

    def init_window(self, title, width, height):
        win = Window(title, width, height)
        self.window = win
        return win

    def run_events(self):
        running = True
        event = sdl2.events.SDL_Event()
        while running:
            while sdl2.events.SDL_PollEvent(ctypes.byref(event)) != 0:
                if event.type == sdl2.events.SDL_QUIT:
                    running = False
                    break

                if event.type == sdl2.events.SDL_MOUSEBUTTONDOWN:
                    running = False
                    break

                if event.type == sdl2.events.SDL_KEYDOWN:
                    if event.key.keysym.sym == sdl2.keycode.SDLK_ESCAPE:
                        running = False

            self.draw()
            self.window.refresh()
            sdl2.timer.SDL_Delay(30)
        self.quit()

    def draw(self):
        if not self.window:
            raise Exception("Window must be initialized (execute init_window() method)")
        self.window.draw()

    def destroy(self):
        self.window.destroy()
        sdl2.SDL_Quit()

    def quit(self):
        self.destroy()
