from _ctypes import byref

import sdl2
from paper.common import renderTexture, loadTexture


class Canvas:
    def __init__(self, renderer):
        self.renderer = renderer
        self.default_color = sdl2.SDL_Color(0, 0, 0, 0)
        self._color = self.default_color
        self._color_stack = []

    @property
    def color(self):
        return self._color

    def push_color(self, color):
        if isinstance(color, tuple):
            color = sdl2.SDL_Color(*color)
        self._color_stack.append(self._color)
        self._color = color
        sdl2.SDL_SetRenderDrawColor(self.renderer,
                                    self.color.r, self.color.g, self.color.b,
                                    sdl2.SDL_ALPHA_OPAQUE)

    def pop_color(self):
        if len(self._color_stack) == 0:
            color = self.default_color
        else:
            color = self._color_stack.pop()
        self._color = color
        sdl2.SDL_SetRenderDrawColor(self.renderer,
                                    color.r, color.g, color.b,
                                    sdl2.SDL_ALPHA_OPAQUE)

    def clear(self):
        sdl2.render.SDL_RenderClear(self.renderer)

    def destroy(self):
        sdl2.render.SDL_DestroyRenderer(self.renderer)

    def snapshot(self, width: int, height: int):
        tex = loadTexture("D:\\develop\\cloud_imitation\\images\\cloud.png", self.renderer)
        texture = sdl2.SDL_CreateTexture(self.renderer,
                                         sdl2.SDL_PIXELFORMAT_ABGR8888, sdl2.SDL_TEXTUREACCESS_STREAMING,
                                         width, height)
        # return texture
        return tex

    def make_texture(self, surface):
        return sdl2.render.SDL_CreateTextureFromSurface(self.renderer, surface)
        # return sdl2.render.SDL_CreateTexture(self.renderer, surface)

    def draw_texture(self, texture, x, y):
        renderTexture(texture, self.renderer, x, y)
        # renderTexture(tex, self.renderer, x, y)

    def draw_line(self, x1: int, y1: int, x2: int, y2: int):
        sdl2.render.SDL_RenderDrawLine(self.renderer, x1, y1, x2, y2)

    def draw_rect(self, x: int = 0, y: int = 0, w: int = 0, h: int = 0, rect=None):
        if rect is None:
            rect = sdl2.SDL_Rect(x, y, w, h)

        sdl2.render.SDL_RenderFillRect(self.renderer, rect)

    def draw_corner_rect(self, x: int = 0, y: int = 0, w: int = 0, h: int = 0, rect=None, radius=2):
        if rect is None:
            rect = sdl2.SDL_Rect(x, y, w, h)
        x = rect.x
        y = rect.y
        w = rect.w
        h = rect.h

        inner_rect = sdl2.SDL_Rect(x + radius, y + radius, w - radius - radius, h - radius - radius)
        sdl2.render.SDL_RenderFillRect(self.renderer, inner_rect)

        for i in range(radius):
            x1 = x + i
            y1 = y + radius - i
            x2 = x + i
            y2 = y + h - radius + i
            sdl2.render.SDL_RenderDrawLine(self.renderer, x1, y1, x2, y2)
            sdl2.render.SDL_RenderDrawLine(self.renderer, x + w - x1 - 1, y1, x + w - x2 - 1, y2)

            x1 = x + radius - i
            y1 = y + i
            x2 = x + w - radius + i
            y2 = y + i
            sdl2.render.SDL_RenderDrawLine(self.renderer, x1, y1, x2, y2)
            sdl2.render.SDL_RenderDrawLine(self.renderer, x1, y + h - y1 - 1, x2, y + h - y2 - 1)


class SoftwareCanvas(Canvas):
    def __init__(self, width: int, height: int):
        surf = sdl2.SDL_CreateRGBSurfaceWithFormat(0, width, height, 32, 10)
        renderer = sdl2.SDL_CreateSoftwareRenderer(surf)
        super().__init__(renderer)
        sdl2.SDL_SetRenderDrawColor(self.renderer, 0xFF, 0xFF, 0xFF, 0xFF);
        sdl2.SDL_RenderClear(self.renderer);
        sdl2.SDL_RenderDrawLine(self.renderer, 3, 3, 10, 20)
