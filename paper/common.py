#!/usr/bin/env python
# -*- coding: utf-8 -*-
from _ctypes import pointer
from ctypes import c_long

import sdl2
import sdl2.ext
from sdl2 import sdlttf as ttf

__author__ = 'cjaym'


def loadTexture(filePath, renderer):
    surf = sdl2.ext.load_image(filePath)
    # texture = ttf.IMG_LoadTexture(renderer, str.encode(filePath))
    texture = sdl2.SDL_CreateTextureFromSurface(renderer, surf)

    if not texture:
        print("SDL_CreateTextureFromSurface")

    return texture


def renderTexture(tex, render, x, y):
    dst = sdl2.SDL_Rect(x, y)

    w = pointer(c_long(0))
    h = pointer(c_long(0))

    sdl2.render.SDL_QueryTexture(tex, None, None, w, h)
    dst.w = w.contents.value
    dst.h = h.contents.value

    sdl2.render.SDL_RenderCopy(render, tex, None, dst)
