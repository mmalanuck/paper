import os
from typing import List

import sdl2
from sdl2 import sdlttf as ttf


class FontQuery:
    DEFAULT_SIZE = 12
    DEFAULT_STYLE = "Regular"

    def __init__(self, ff, size: int = None, style: str = None):
        self.font_fabrick = ff
        self._size = size or FontQuery.DEFAULT_SIZE
        self._style = style or FontQuery.DEFAULT_STYLE

    def size(self, font_size: int):
        self._size = font_size
        return self

    def style(self, font_style: str):
        self._style = font_style
        return self

    def get(self):
        return self.font_fabrick.get(self)

    def get_size(self):
        return self._size

    def get_style(self):
        return self._style


class Font:
    def __init__(self, filename: str, size: int = 12):
        wi = ttf.TTF_WasInit()
        if not wi:
            raise Exception("TTF library not initialized")

        self.color = sdl2.SDL_Color(255, 255, 255, 255)
        self.back_color = sdl2.SDL_Color(0, 0, 0, 0)
        if not os.path.exists(filename):
            raise FileNotFoundError("Шрифт '%s' не найден" % filename)

        self.font = ttf.TTF_OpenFont(str.encode(filename), size)
        if not self.font:
            if self.font is not None:
                raise Exception(ttf.TTF_GetError())
            else:
                raise Exception("Font not initialized")

    def render(self, message, canvas):
        canvas.push_color(self.color)
        surf = canvas.make_text_texture(self.font, message, self.back_color)
        canvas.pop_color()
        texture = sdl2.render.SDL_CreateTextureFromSurface(canvas.renderer, surf)
        if not texture:
            raise Exception("Error while texture creation")

        sdl2.surface.SDL_FreeSurface(surf)
        return texture

    def destroy(self):
        ttf.TTF_CloseFont(self.font)


class FontFabric:
    def __init__(self, name: str, dir_path: str):
        self.name = name
        self.dir_path = dir_path
        self._cached_fonts = {}

    def loaded_fonts(self):
        for fonts in self._cached_fonts.values():
            for font in fonts.values():
                yield font

    def size(self, font_size: int):
        return FontQuery(self, font_size)

    def style(self, font_style: str):
        return FontQuery(self, style=font_style)

    def get(self, fq: FontQuery = None):
        size = FontQuery.DEFAULT_SIZE
        style = FontQuery.DEFAULT_STYLE
        if fq:
            size = fq.get_size()
            style = fq.get_style()

        fonts = self.get_fonts(style)
        font = self.get_font_size(size, style, fonts)
        return font

    def get_font_size(self, size: int, style: str, fonts: List):
        if size not in fonts:
            file_name = "%s-%s.ttf" % (self.name, style)
            font_file = os.path.join(self.dir_path, file_name)
            font = Font(font_file, size)
            fonts[size] = font
        return fonts[size]

    def get_fonts(self, style: str):
        if style not in self._cached_fonts:
            self._cached_fonts[style] = {}
        return self._cached_fonts[style]


class RobotoFontFabric(FontFabric):
    def __init__(self, dir_path: str):
        super().__init__("Roboto", dir_path)

    @property
    def Thin(self):
        return FontQuery(self, style="Thin")

    @property
    def Light(self):
        return FontQuery(self, style="Light")

    @property
    def Regular(self):
        return FontQuery(self, style="Regular")

    @property
    def Medium(self):
        return FontQuery(self, style="Medium")

    @property
    def Bold(self):
        return FontQuery(self, style="Bold")

    @property
    def Black(self):
        return FontQuery(self, style="Black")

    @property
    def ThinItalic(self):
        return FontQuery(self, style="ThinItalic")

    @property
    def LightItalic(self):
        return FontQuery(self, style="LightItalic")

    @property
    def MediumItalic(self):
        return FontQuery(self, style="MediumItalic")

    @property
    def BoldItalic(self):
        return FontQuery(self, style="BoldItalic")

    @property
    def BlackItalic(self):
        return FontQuery(self, style="BlackItalic")


class FontManager:
    def __init__(self, fonts_dir: str):
        self.fonts_dir = fonts_dir
        tfi = ttf.TTF_Init()
        if tfi != 0:
            raise Exception("TTF initialization failed")

    def get_font_fabric(self, font_name: str):
        ff = FontFabric(font_name, self.fonts_dir)
        return ff

    def roboto(self):
        return RobotoFontFabric(self.fonts_dir)
