from _ctypes import byref

import sdl2
import sdl2.sdlttf as ttf

from paper import Canvas
from paper.common import loadTexture
from paper.fonts import Font, FontFabric
from paper.widgets import Widget


class Button(Widget):
    def __init__(self, text: str, ff: FontFabric):
        super().__init__()
        self._text = ""
        self.ff = ff
        self.background = sdl2.SDL_Color(100, 100, 100)
        self.bt = sdl2.SDL_Color(100, 100, 0)
        self.foreground = sdl2.SDL_Color(255, 255, 255)

        self._text_y = 0
        self.text_texture = None
        self.text = text

    @property
    def text(self):
        return self._text

    @text.setter
    def text(self, value):
        if self.text == value:
            return
        self._text = value
        font = self.ff.size(14).get()
        text_surf = ttf.TTF_RenderUTF8_Shaded(font.font, str.encode(self.text), self.foreground, self.background)
        self.update_size(text_surf.contents.w, text_surf.contents.h)
        self.recreate_canvas()
        self.text_texture = self.canvas.make_texture(text_surf)
        sdl2.surface.SDL_FreeSurface(text_surf)

        info = sdl2.SDL_RendererInfo()
        ret = sdl2.SDL_GetRendererInfo(self.canvas.renderer, byref(info))
        print(info.name)
        print(info.flags)
        print(info.num_texture_formats)
        print(info.texture_formats)
        print(info.max_texture_width)
        print(info.max_texture_height)
        print("-----------")

    @property
    def rect(self):
        if self._rect is None:
            self._rect = sdl2.SDL_Rect(self.x, self.y, self._width, self._height)
        return self._rect

    def draw_cache(self):
        self.canvas.push_color(self.background)
        self.canvas.draw_corner_rect(rect=self.rect)
        self.canvas.pop_color()
        self.canvas.push_color(self.foreground)
        self.canvas.draw_texture(self.text_texture, self.x + 16, self.y + self._text_y)
        self.canvas.pop_color()
        self.canvas.push_color((200, 255, 0))
        self.canvas.draw_line(30, 10, 20, 80)
        self.canvas.pop_color()

    def draw(self, renderer: Canvas):
        self.draw_cache()
        if self.cached_texture is None:
            self.update_cache()
        renderer.draw_texture(self.cached_texture, self.x, self.y)
        renderer.push_color((200, 255, 0))
        renderer.draw_line(0, 0, 100, 100)
        renderer.pop_color()

    def destroy(self):
        sdl2.render.SDL_DestroyTexture(self.text_texture)

    def update_size(self, w, text_height):
        self._width = 16 + w + 16
        self._height = 32

        self._text_y = text_height // 2
        self._rect = None
