import sdl2
from paper.fonts import Font
from paper.widgets import Widget


class Label(Widget):
    def __init__(self, text: str, font: Font):
        super().__init__()
        self.text = text
        self.font = font

    def draw(self, canvas):
        if self.image is None:
            self.image = self.font.render(self.text, canvas)
        canvas.draw_texture(self.image, self.x, self.y)

    def destroy(self):
        sdl2.render.SDL_DestroyTexture(self.image)
