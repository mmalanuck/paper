import sdl2

from paper.canvas import SoftwareCanvas
from paper.common import loadTexture


class Widget:
    def __init__(self):
        self.x = 0
        self.y = 0
        self._width = 0
        self._height = 32
        self._rect = sdl2.SDL_Rect(0, 0, 32, 32)
        self.canvas = None
        self.cached_texture = None

    @property
    def width(self):
        return self._width

    @property
    def height(self):
        return self._height

    @property
    def rect(self):
        return self._rect

    def recreate_canvas(self):
        if self.canvas:
            self.canvas.destroy()
        self.canvas = SoftwareCanvas(self.width, self.height)
        self.cached_texture = None

    def draw(self, canvas):
        raise NotImplementedError('This method must be overrided in subclass')

    def destroy(self):
        pass

    def update_cache(self):
        self.cached_texture = self.canvas.snapshot(self.width, self.height)
