#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sdl2

from paper import Canvas


class Window:
    def __init__(self, title, width, height):
        self.x = 0
        self.y = 0
        self.width = width
        self.height = height
        self.title = title
        self.widget = None

        self.window = sdl2.video.SDL_CreateWindow(str.encode(title), 100, 100,
                                                  self.width, self.height,
                                                  sdl2.video.SDL_WINDOW_SHOWN | sdl2.video.SDL_WINDOW_RESIZABLE)

        renderer = sdl2.render.SDL_CreateRenderer(self.window, -1, sdl2.render.SDL_RENDERER_ACCELERATED)
        self.canvas = Canvas(renderer)

    def set_widget(self, widget):
        self.widget = widget

    def draw(self):
        self.canvas.clear()
        if self.widget is not None:
            self.widget.draw(self.canvas)

    def refresh(self):
        sdl2.render.SDL_RenderPresent(self.canvas.renderer)

    def destroy(self):
        self.canvas.destroy()
        sdl2.video.SDL_DestroyWindow(self.window)
