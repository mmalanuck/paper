import os

cur_dir = os.path.dirname(os.path.abspath(__file__))
dlls_dir = os.path.join(cur_dir, os.pardir, "dlls")
os.environ["PYSDL2_DLL_PATH"] = dlls_dir
