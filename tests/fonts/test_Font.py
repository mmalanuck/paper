import os
import pytest
import sdl2.sdlttf as ttf

from paper.fonts import Font
from tests.test_fonts import font_dir


def setup():
    while ttf.TTF_WasInit():
        ttf.TTF_Quit()


def teardown():
    while ttf.TTF_WasInit():
        ttf.TTF_Quit()


def test_library_not_initialized__exception():
    assert ttf.TTF_WasInit() == False
    with pytest.raises(Exception) as exc:
        font = Font("any_path")
    assert "TTF library not initialized" == exc.value.args[0]


def test_library_initialized__no_exception():
    tfi = ttf.TTF_Init()

    font_path = os.path.join(font_dir, "Roboto-Bold.ttf")
    font = Font(font_path)
    assert font is not None


def test_font_finded__ok():
    ttf.TTF_Init()
    font_path = os.path.join(font_dir, "Roboto-Bold.ttf")
    font = Font(font_path)
    assert font is not None


def test_font_not_founded__raise_exception():
    ttf.TTF_Init()
    font_path = os.path.join(font_dir, "MISSED.ttf")
    with pytest.raises(FileNotFoundError) as exc:
        font = Font(font_path)
