from paper.fonts import FontManager
from tests.test_fonts import font_dir


def test_initialy_fonts_are_nol_loaded():
    fm = FontManager(font_dir)
    ff = fm.get_font_fabric("Roboto")
    assert 0 == sum(1 for _ in ff.loaded_fonts())


def test_secondary_loading__not_added_new_fonts():
    fm = FontManager(font_dir)
    ff = fm.get_font_fabric("Roboto")
    ff.size(12).get()
    ff.size(12).get()
    ff.size(12).get()
    ff.get()
    ff.style("Regular").get()
    ff.style("Regular").size(12).get()

    assert 1 == sum(1 for _ in ff.loaded_fonts())


def test_load_any_size__increase_loaded_fonts():
    fm = FontManager(font_dir)
    ff = fm.get_font_fabric("Roboto")
    ff.size(10).get()
    ff.size(11).get()
    ff.size(12).get()
    assert 3 == sum(1 for _ in ff.loaded_fonts())


def test_load_any_styles__increase_loaded_fonts():
    fm = FontManager(font_dir)
    ff = fm.get_font_fabric("Roboto")
    ff.style("Bold").get()
    ff.style("Light").get()
    ff.style("Regular").get()
    ff.style("Thin").get()
    assert 4 == sum(1 for _ in ff.loaded_fonts())


def test_load_any_styles_and_sizes__increase_loaded_fonts():
    fm = FontManager(font_dir)
    ff = fm.get_font_fabric("Roboto")
    ff.style("Bold").size(12).get()
    ff.style("Bold").size(14).get()
    ff.style("Bold").get()

    ff.style("Light").size(10).get()
    ff.style("Light").size(11).get()
    ff.style("Light").size(12).get()

    ff.style("Regular").size(12).get()
    ff.style("Regular").size(12).get()

    ff.style("Thin").size(10).get()
    ff.style("Thin").size(12).get()
    ff.style("Thin").size(12).get()
    assert 8 == sum(1 for _ in ff.loaded_fonts())
