from paper.fonts import FontQuery


def test_initialy__values_are_default():
    fq = FontQuery(None)
    assert fq.get_size() == FontQuery.DEFAULT_SIZE
    assert fq.get_style() == FontQuery.DEFAULT_STYLE


def test_set_size__change_size():
    fq = FontQuery(None, 18)
    assert fq.get_size() == 18
    assert fq.get_style() == FontQuery.DEFAULT_STYLE


def test_set_style__change_style():
    fq = FontQuery(None, style='thin')
    assert fq.get_size() == 12
    assert fq.get_style() == "thin"


def test_change__style_and_sze__ok():
    fq = FontQuery(None, 18, 'bold')
    assert fq.get_size() == 18
    assert fq.get_style() == "bold"
