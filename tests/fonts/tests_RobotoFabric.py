from paper.fonts import FontManager
from tests.test_fonts import font_dir


def test_get_thin__ok():
    fm = FontManager(font_dir)
    roboto = fm.roboto()
    query = roboto.Thin.size(10)
    assert query.get_style() == "Thin"


def test_get_light__ok():
    fm = FontManager(font_dir)
    roboto = fm.roboto()
    query = roboto.Light.size(10)
    assert query.get_style() == "Light"


def test_get_regular__ok():
    fm = FontManager(font_dir)
    roboto = fm.roboto()
    query = roboto.Regular.size(10)
    assert query.get_style() == "Regular"


def test_get_medium__ok():
    fm = FontManager(font_dir)
    roboto = fm.roboto()
    query = roboto.Medium.size(10)
    assert query.get_style() == "Medium"


def test_get_bold__ok():
    fm = FontManager(font_dir)
    roboto = fm.roboto()
    query = roboto.Bold.size(10)
    assert query.get_style() == "Bold"


def test_get_black__ok():
    fm = FontManager(font_dir)
    roboto = fm.roboto()
    query = roboto.Black.size(10)
    assert query.get_style() == "Black"


def test_get_thin_italic__ok():
    fm = FontManager(font_dir)
    roboto = fm.roboto()
    query = roboto.ThinItalic.size(10)
    assert query.get_style() == "ThinItalic"


def test_get_medium_italic__ok():
    fm = FontManager(font_dir)
    roboto = fm.roboto()
    query = roboto.MediumItalic.size(10)
    assert query.get_style() == "MediumItalic"


def test_get_bold_italic__ok():
    fm = FontManager(font_dir)
    roboto = fm.roboto()
    query = roboto.BoldItalic.size(10)
    assert query.get_style() == "BoldItalic"


def test_get_black_italic__ok():
    fm = FontManager(font_dir)
    roboto = fm.roboto()
    query = roboto.BlackItalic.size(10)
    assert query.get_style() == "BlackItalic"
