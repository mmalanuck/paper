import pytest
import sdl2

from paper import Canvas


def test_init_canvas():
    canvas = Canvas(None)
    assert canvas is not None


def test_default_color():
    canvas = Canvas(None)
    assert canvas.color == canvas.default_color


def test_push_color__change_color():
    canvas = Canvas(None)

    colors = [
        (10, 10, 10),
        (50, 50, 50),
        (100, 100, 100),
        (255, 255, 255)
    ]
    for color in colors:
        canvas.push_color(color)
        assert canvas.color == sdl2.SDL_Color(*color)


def test_pop_color__ok():
    canvas = Canvas(None)

    colors = [
        (10, 10, 10),
        (50, 50, 50),
        (100, 100, 100),
        (255, 255, 255)
    ]
    for color in colors:
        canvas.push_color(color)

    colors.reverse()
    for expected_color in colors:
        assert canvas.color == sdl2.SDL_Color(*expected_color)
        canvas.pop_color()
