import os
import pytest
from paper.fonts import FontManager, FontQuery, Font

proj_dir = os.path.dirname(os.path.abspath(__file__))
proj_dir = os.path.join(proj_dir, os.pardir)
proj_dir = os.path.abspath(proj_dir)
font_dir = os.path.join(proj_dir, "fonts")


# Example
def test_example__font_fabrick():
    fm = FontManager(font_dir)
    ff = fm.get_font_fabric("Roboto")
    font_query = ff.size(22)
    font = font_query.style("thin").get()
    assert font is not None


def test_example__roboto():
    fm = FontManager(font_dir)
    roboto = fm.roboto()
    font_query = roboto.size(22)
    font = font_query.style("thin").get()
    assert font is not None
